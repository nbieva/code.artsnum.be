![](http://rhuthmos.eu/IMG/jpg/8aurelie_nemours.jpg)

[Aurélie Nemours](https://www.google.be/search?bih=825&biw=1913&tbm=isch&sa=1&ei=HzG6W7bQI4mysAeuurPACA&q=aur%C3%A9lie+nemours&oq=aur%C3%A9lie+nemours&gs_l=img)

Après avoir réalisé un damier simple, tentez d'utiliser les boucles pour créer, sur base d'une grille, une série de variantes.
N'oubliez pas que vous pouvez chevaucher vos éléments.
Vous pouvez aussi varier les modes de fusion en utilisant la fonction blendMode().
N'oubliez pas d'exporter vos images pour garder des traces de vos essais.

## Part 1
A partir de ce que nous avons vu, créez un damier de 800x800px.
Vous devrez pour ce faire utiliser les variables et les conditions.

## Part 2
Une fois la logique déterminée, poussez le travail plus loin. Comment pourriez-vous faire évoluer/animer/modifier ce damier?
Songez à décaler vos éléments, et à explorer les [possibilités de transparence](https://processing.org/reference/blendMode_.html) qu'offre Processing..
Créez entre 1 et 3 versions et exportez-les au format PNG.
Conservez, pour chacune le code de votre sketch de façon à pouvoir y revenir si nécessaire.
Utilisez la référence Processing (et le web)

## Part 3
Dans votre premier damier, un carré est ajouté à chaque boucle draw(). Comment pourriez-vous vous y prendre pour que le damier apparaisse en une seule fois.

[Les répétitions(boucles)](https://code.artsnum.be/part2/boucles.html)

## Caractéristiques techniques
- Format de 800x800px
- Exports PNG

## Concepts et fonctions utilisés:
+ Notions précédentes (livre do tempo)
+ Draw(), la boucle
+ Fonctions
+ Variables
+ Boucles (et éventuellement boucles impriquées)
+ Conditions
+ Export au format PNG (save() )

## Fonctions principales
size()
rect()
fill()
stroke()
noStroke()
strokeWeight()
random()


## Références

+ [Bauhaus textiles](https://www.google.be/search?q=bauhaus+textiles&espv=2&biw=1784&bih=1320&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwiQroqMoMzQAhWL0hoKHQkQAPEQsAQIHQ)
+ [Aurélie Nemours](https://www.google.be/search?q=aur%C3%A9lie%20nemours&tbm=isch&tbs=rimg%3ACb-80AQDLBKvIjhYmeFH1CHasMhogVyD2QoicaynEl_1hcYpcrfGcdSCvblt1zmANkG0t3yCd1g0PjT3qIzSJoBwrCyoSCViZ4UfUIdqwEaI3m5k_11sU7KhIJyGiBXIPZCiIRPzjwBT5WvhkqEglxrKcSX-FxihGup-CpwVOaWSoSCVyt8Zx1IK9uEWiu8lx6x9qDKhIJW3XOYA2QbS0Rx5wb4o-yfqoqEgnfIJ3WDQ-NPRERXU90OZq3ESoSCeojNImgHCsLEbrClHF_1_1FGL&tbo=u&bih=803&biw=1295&ved=0ahUKEwjG89n97fDPAhUD1xoKHUjFBacQ9C8ICQ&dpr=1#imgrc=_)
+ Damien Hirst, [Spot paintings](https://www.google.be/search?q=damien+hirst&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiUppPmoKvSAhULB8AKHZYRAZAQ_AUICCgB&biw=2266&bih=1194#tbm=isch&q=damien+hirst+spot+paintings&*)

# Variations

![](https://github.com/lam-artsnum/code/wiki/images/grid-01.png)

![](https://github.com/lam-artsnum/code/wiki/images/grid-02.png)

![](https://github.com/lam-artsnum/code/wiki/images/grid-03.png)

![](https://github.com/lam-artsnum/code/wiki/images/grid-04.png)

![](https://github.com/lam-artsnum/code/wiki/images/grid-05.png)

![](https://github.com/lam-artsnum/code/wiki/images/grid-06.png)

![](https://github.com/lam-artsnum/code/wiki/images/grid-07.png)
