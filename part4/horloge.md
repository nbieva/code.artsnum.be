#Horloge


```processing
void setup() {
  size(800,600);
  noStroke();
}

void draw() {
  background(0, 122, 142);
  fill(76, 165, 71);
  rect(0,0,hour()*(width/24),height/3);
  fill(61, 121, 87);
  rect(0,height/3,minute()*(width/60),height/3);
  fill(64, 94, 104);
  rect(0,2*(height/3),second()*(width/60),height/3);
}
```