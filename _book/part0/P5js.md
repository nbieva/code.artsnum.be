#P5js

![P5js](/assets/p5js2.png)

> Notes sur ce qu'est P5js (et javascript), les différence avec Processing. Les avantages ou inconvénients de l'un ou de l'autre.

P5js est une bibliothèque javaScript en filiation directe avec Processing. 
Tout en conservant l'esprit de Processing, la simplicité de sa syntaxe et ses objectifs pédagogique et artistique, elle permet d'exécuter des scripts directement dans les pages web (le code étant exécuté par votre navigateur).

Comme P5js est une bibliothèque javaSCript, elle permet également de manipuler directement les éléments HTML ou styles CSS et offre donc de nouvelles posssibilités d'interactivité.

Vous trouverez plus d'informations sur ce projet (référence, exemples, ..) sur le site de P5js : https://p5js.org/

Si vous travaillez avec le très pratique [éditeur de P5js en ligne](https://editor.p5js.org/), vous noterez certaines différences par rapport à Processing: 

* Les variables sont notées **var** (à la place de int ou float ou autre)
* **function** à la place de void
* **createCanvas** à la place de size

> https://editor.p5js.org/

Néanmoins, vous passerez sans souci de l'un à l'autre, dans un premier temps du moins.

P5js est un projet de Lauren McCarthy, artiste, mais est maintenant développée et maintenue, par une communauté de contributeurs et d'utilisateurs (remantant les éventuels bugs).
