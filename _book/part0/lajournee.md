#Processing

>Comme dans n'importe quel dessin, l'ordre des opérations est important pour déterminer le rendu final. Ce principe s'applique même en dehors de Processing: « S'habiller, sortir, aller au travail » ne donnera pas le même résultat que « sortir, aller au travail, s'habiller ». (Floss Manuals, Processing)

Processing est particulièrement intéressant dans le cadre d'une journée comme celle-ci.

+ Il nous permet de découvrir dans un environnement 'autonome' les logiques de base du code et de la programmation.
+ Il permet un retour visuel rapide. On obtient assez rapidement des résultats convainquants.
+ Il concerne tout particulièrement les arts visuels (mais pourra aussi vous servir si vous êtes intéressés par le son, le texte ou le web par exemple)
+ C'est un outil opensource, libre (et gratuit, et vous êtes aussi libres de faire un don)
+ Il nous initie également au partage (de créations, de ressources) et nous apprend à apprendre des autres.
+ de par la grande variété des applications possibles, il concerne potentiellement chacune des options de l'école, et chacun.e d'entre vous.

![](http://nicolasbieva.be/wp-content/uploads/2016/10/svg.png)
[SVG](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics)

# La journée



+ Les objectifs du workshop
+ le déroulement de la journée
+ Qu'entend-t-on par 'code' ?
+ [[Pourquoi différent langages? Un rapide panorama des principaux langages dont vous entendrez parler: HTML, CSS, Javascript, PHP, Python.. mais aussi Graphviz (.dot), Markdown, MediaWiki...|Hello world]];
+ Code et création visuelle: quelques cas et exemples
+ [Sol Le witt](https://www.google.be/search?espv=2&biw=1020&bih=803&tbm=isch&sa=1&q=sol+le+witt+wall+drawing+&oq=sol+le+witt+wall+drawing+&gs_l=img.3...13444.13444.0.13581.0.0.0.0.0.0.0.0..0.0....0...1c.1.64.img..0.0.0.4xKywaEGZVc), Baldessari, [Morellet](https://www.youtube.com/watch?v=Cd7-rJ-K0Dc).. (6 répartitions aléatoires de 4 carrés noirs et blanc d'après les chiffres pairs et impairs du nombre PI)+ [Allan McCollum](http://theshapesproject.com/)- [The Shapes project](http://www.micheledidier.com/index.php/fr/mfc-expositions/mfc-archives/mfc-exposition-allan-mccollum.html)+ [ceci](http://allanmccollum.net/allanmcnyc/gasparina/mccollum_gasparina.html)
+ Concrètement: Les outils du workshop
+ page web, memo
+ Les outils, éditeurs etc.
+ Les ressources (Référence Processing, Floss Manuals, Daniel Shiffman..)
+ Showcase ([Prototyp-o](https://framablog.org/2014/04/22/prototypo-vos-polices-sur-mesure/) + [ici](https://www.prototypo.io/), [MIT id](http://www.thegreeneyl.com/mit-media-lab-identity-1), [Fragmanted memory](https://phillipstearns.wordpress.com/fragmented-memory/), [Fluid Leaves](http://reinoudvanlaar.nl/project/leavespattern/), [Ball droppings](http://balldroppings.com/js/), [COP15](http://www.okdeluxe.co.uk/cop15/), [Rafael Rozendaal](http://www.newrafael.com/complex-computational-compositions/),Etc..+ Particles..)
+ Les [bibliothèques](https://processing.org/reference/libraries/)
+ [References](https://processing.org/reference/), [[ressources]] et documentation. [Floss](https://www.flossmanualsfr.net/), [Open Processing](https://www.openprocessing.org/), [Learning Processing](http://learningprocessing.com/examples/chp05/example-05-03-rollovers)
+ Mise en place (téléchargement, interface, référence, ressources...).[Processing 3.0](https://github.com/processing/processing/wiki/Changes-in-3.0) .


***


![](http://investigacion.itacabcn.com/img/maeda.jpg)
Design by numbers

# Processing

### Introduction
+ Processing et P5js, une filiation.. ([Processing](https://en.wikipedia.org/wiki/Processing_(programming_language)), [P5js](https://p5js.org/reference/), [Arduino (Genuino)](https://blog.arduino.cc/wp-content/uploads/2016/03/after.png)..) [Paul Rand](https://en.wikipedia.org/wiki/Paul_Rand) / [Ikko Tanaka](https://www.google.be/search?q=ikko+tanaka&espv=2&biw=2550&bih=1320&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjLuY-t1srPAhWJrxoKHdyPAagQ_AUIBigB) > John Maeda (Design by numbers) > Ben Fry & Casey Reas (Processing) > Lauren McCarthy (P5js)
+ Qu'est-ce que Processing?
+ Installer Processing (+ versions)
+ Principes de base
+ L'interface (programme, console, canevas..)
+ Structure d’un programme (Setup et draw)
+ La syntaxe et les commentaires
+ Les coordonnées

```processing
//Déclaration des variables

void setup() {
//Initialisation du programme. Se joue une seule fois.
}

void draw() {
//Boucle du programme. Se joue en boucle.
}
```
Ou, un peu plus complexe:

```processing
// Déclaration des variables
int largeur;
int hauteur;

//Initialise le programme
void setup() {
size(600,400);
// Assignation de valeurs aux variables
largeur = 50;
hauteur = 25;
noStroke();
}

// Boucle du programme
void draw() {
rectMode(CENTER);
rect(width/2, height/2, largeur, hauteur);
if (keyPressed) {
largeur += 1;
hauteur += 1;
} else if (mousePressed) {
largeur -= 1;
hauteur -= 1;
}
}
```
![La boucle draw](https://www.flossmanualsfr.net/processing/_booki/processing/static/Processing-Draw-illustration_draw-fr-old.png)

![](https://twenty2.net/wordpress/wp-content/uploads/2012/08/8_22_12_sol_sign.jpg)

### Formes simples
+ Le système de coordonnées
+ Formes simples et premières variables (line, rect, ellipse..) + [Shapes](https://processing.org/reference/beginShape_.html)
+ Courbes de Bezier
+ La console (print, println)

```processing
void setup() {
    size(600,400);
    fill(0);
}

void draw() {
    //println("Position : "+mouseX+", "+mouseY);
    text(mouseX+", "+mouseY,mouseX, mouseY);
}
```


