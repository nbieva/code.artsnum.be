>"Drawing is a verb"
> [Richard Serra](https://www.moma.org/explore/inside_out/2011/10/20/to-collect/)

{{ "https://player.vimeo.com/video/74725118" | video }}

Comme vous le savez tous, les outils numériques sont en constante évolution, en ce compris le code. Les langages, leurs fonctionalités et leurs finalités, sont [très variés](https://esolangs.org/wiki/Language_list).

Le workshop est organisé sur une journée et vise à **vous familiariser **avec le code à travers une série d'explorations visuelles.
Sur base de quelques propositions simples, vous serez amenés à formuler et structurer vos idées de façon à pouvoir les développer d'une façon nouvelle, grâce à la programmation.
Au delà de cette familiarisation au code, vous aurez à l'issue du workshop une idée claire sur les **méthodes **à mettre en œuvre et les **ressources **à votre disposition pour poursuivre le travail de façon autonome.

![](http://md1.libe.com/photo/662299-card31.jpg?modified_at=1405084608&width=750)

https://i0.wp.com/www.guggenheim.org/wp-content/uploads/2015/02/art-on-kawara-telegram-to-sol-lewitt-i-am-still-alive.jpg?w=870

![](/assets/stillalive.png)

https://www.youtube.com/watch?v=RDrHHsez3nU

Cette première approche du code, laboratoire et expérimentale, se fera à l'aide de [Processing](https://www.processing.org/), logiciel libre, créé par des artistes, pour des artistes, dans les champs pédagogique et visuel.
Très visuel lui-même, Processing permet d'entrer facilement dans les **logiques d'un algorithme **et d'en comprendre les fonctions de base \(variables, boucles, conditions, fonctions, listes..\)

Manipulation et la génération de textes, traitement de vidéos en temps réel, design génératif, pilotage de machines, gestion d'installations interactives, génération de pages web à la volée ou de motifs, impression 3D, création d'outils de dessin ou de manipulation d'images bitmap, son, création de fontes, traitement de données...  
[Tous ces procédés](https://processing.org/reference/libraries/) et domaines sont susceptibles de faire appel à des compétences, même basiques, en matière de code, et peuvent être servis par un logiciel comme Processing.

{{ "https://www.youtube.com/watch?v=c4cgB4vJ2XY" | video }}

Notre souhait est que cette journée puisse être, pour tous, une porte ouverte vers de **nouvelles possibilités**, et puisse enrichir votre travail et votre démarche personnelle, vous amenant à une **plus grande autonomie**, à la fois technique et artistique.

Il est important de noter que **ce workshop s'adresse à tous **les étudiants de B1, quelque soit leur option, ou leur niveau de connaissance en la matière. Il a été conçu comme tel.