<p class="lead">En règle générale, essayez de nous rendre tout ce que vous réalisez durant cette journée, ou du moins tout ce que vous pensez être significatif. Pour Processing, rassemblez le tout dans un dossier. Pour P5js, le partage du travail se fait exclusivement en ligne.</p>

<div class="notification danger">
    <p>En fin de journée, ne partez surtout pas sans vous être assuré que vos fichiers sont bien en notre possession et que vous avez rempli le <a href="https://code.artsnum.be/part0/formulaire.html">formulaire de feedback</a>.</p>
</div>

#Processing

+ Créez un dossier dans votre dossier "Documents" et nommez-le comme suit:**nom-prenom-option**
+ n'utilisez pas d'espace, d'accents ou de caractères spéciaux dans vos noms de fichiers ou de dossiers.
+ En cours de journée, lorsqu'un travail est terminé ou qu'une étape importante est franchie, déplacez vos fichiers vers ce répertoire.
+ En fin de journée, connectez-vous au réseau (voir ci-dessous), et déplacez votre dossier portant votre nom vers le répertoire commun (voir ci-dessous) demandez de l'aide si nécessaire.

## Pour se connecter au réseau local:

Dans le finder > Aller > Se connecter au serveur > Parcourir > cherchez le poste **HD45** (demandez si la référence est toujours à jour) et déposez votre dossier dans le répertoire **WORKSHOP-PROCESSING** (ou autre qui y ressemble..), qui devrait être présent dans le répertoire **Documents**.

> **Résumé:** HD45 > étudiant > Documents > WORKSHOP-PROCESSING

#P5js

+ Dans votre compte, sur https://editor.p5js.org/, récupérez (copiez) l'URL de votre sketch (File > Share > Present)
+ Collez-là dans ce [formulaire](https://goo.gl/forms/5oG9d03OYwoaMFP72) , avec vos noms correctement orthographiés.
+ L'opération est à répéter pour chaque Sketch
+ Vous pouvez également parcourir les travaux des autres (et vérifier ceux que vous avez remis) dans [ce fichier](https://docs.google.com/spreadsheets/d/1OZXDw84rCJ9c2ewtcP3gLbH4v6Bra8eM6e7dVbDPgqY/edit?usp=sharing).

----

L'ensemble de votre production du jour est à nous remettre.

Tous vos travaux seront ajoutés à la page web reprenant l'ensemble des productions des étudiants, et seront montrés entre autres aux portes ouvertes de l'école.