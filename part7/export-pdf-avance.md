P
our exporter un pdf, il est nécessaire d'importer une bibliothèque \(library\) spécifique. En informatique, une bibliothèque est un ensemble de fonctions utilitaires, regroupées et mises à disposition afin de pouvoir être utilisées sans avoir à les réécrire.

Pour importer une bibliothèque , on utilise la fonction suivante:

```
import processing.nom_de_la_bibliothèque;
```

# Exporter une page simple

```processing
import processing.pdf.*; // importation de la bibliothèque pdf
String titre = "Hello world";

void setup() {
    size(500, 800, PDF, "titre.pdf"); // le format du pdf et son titre
    background(255);
}

void draw() {
    textSize(40); // taille du texte
    fill(0); // couleur du texte
    text(titre, 50, 80); // on affiche le texte

    println("Finished."); // indique dans la console que le pdf est généré
    exit(); // Pas besoin d'affichage, on quitte le programme
}
```

## Exporter plusieurs pages

```
import processing.pdf.*; // importation de la bibliothèque pdf
```

```processing
import processing.pdf.*; // importation de la bibliothèque pdf
String titre = "Hello world";

void setup() {
    size(500, 800, PDF, "pages.pdf"); // le format du pdf et son titre
    background(255);
}

void draw() {
    textSize(40); // taille du texte
    fill(0); // couleur du texte
    text(titre + ' ' + frameCount, 50, 80); // on affiche le texte + la page courante

    PGraphicsPDF pdf = (PGraphicsPDF) g; // Élément de la bibliothèque pour gérer plusieurs pages

    // Si on a atteint 10 pages, on arrête le programme
    if (frameCount == 10) {
        exit();
    } else {
        // Sinon, on passe à la page suivante
        pdf.nextPage(); // Tell it to go to the next page
    }
}
```



