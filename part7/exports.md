#Exports

## En PNG \(bitmap\)

Il existe différentes façon d'exporter une image. La fonction "save" est la plus simple à mettre en oeuvre. Veillez cependant à la placer au bon endroit dans votre sketch. Ce que vous dessinez après la commande ne sera pas exporté.

```processing
save("diagonal.jpg");
```
Vous pouvez également conditionner l'export d'une image, à une pression sur la barre d'espace par exemple. Ceci peut vous permettre de ne pas multiplier les fichiers inutiles.

```processing
if (keyPressed) {
    save("ellipse-"+frameCount+".png");
}
```

[https://processing.org/reference/keyPressed.html](https://processing.org/reference/keyPressed.html)

Attention: cette dernière façon de faire ne fonctionnera que si la condition peut être vérifiée et qu'une boucle est donc active..

```processing
void setup() {
    size(600,400);
}

void draw() {
    background(255);
    rect(50,50,mouseX,mouseY);
}

void keyPressed() {
    save("nomPrenom-option.png");
    exit();
}
```

Dans cet exemple également, chaque fichier enregistré portera le nom "diagonal.png" et écrasera le précédent sans aucune forme d'avertissement.

Afin de pouvoir enregistrer votre fichier sous un nom différent chaque fois que vous pressez une touche \(ou à chaque boucle, ou chaque fois que vous le désirez..\), il vous faudra y intégrer l'une ou l'autre variable. La solution suivante pourrait être une solution:

```processing
void setup() {
    size(600,400);
}
void draw() {
    background(255);
    rect(50,50,mouseX,mouseY);
}
void keyPressed() {
    save("nomPrenom-"+hour()+"-"+minute()+"-"+second()+".png");
    exit();
}
```

Ce qui devrait donner **doeJohn-10-22-56.png** et a toutes les chances d'être différent de vos autres fichiers.

* [Output vers un fichier txt](https://processing.org/reference/createWriter_.html)

Ex. Texte

