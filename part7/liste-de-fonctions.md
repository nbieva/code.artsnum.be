#
General
setup()
draw()
size()
frameRate()
delay()
smooth()
loop()
noLoop()
print()
println()
stop()
exit()

#Dessiner
rect()
ellipse()
triangle()
line()
quad()
point()
beginShape()
vertex()
endShape()
bezier()
rectMode()
ellipseMode()

#Colorer
fill()
stroke()
noFill()
noStroke()
strokeWeight()
Alpha transparency

#Transformer
rotate()
scale()
translate()
pushMatrix()
popMatrix()

#Calculer
random()
noise()
round()
dist()
map()

#Texte
text()
createFont()
#Exporter
save()
beginRecord()
endRecord()

#Interagir
keyPressed()
mousePressed()

#Variables
Déclaration
Type (int, float, boolean, string..)
Assignation
Utilisation


#Variables natives
width, height
mouseX, mouseY
pmouseX, pmouseY
frameCount

#Conditions
if, else if, else

#Comparer
!= (différent)
< (plus petit que)
<= (plus petit ou égal à)
== (égal)
">" (plus grand que)
">=" (plus grand ou égal à)

#Opérateurs logiques
Logical Operators
! (logical NOT)
&& (logical AND)
|| (logical OR)

#Boucles
for, while
