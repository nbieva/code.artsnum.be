# Outils/softwares

* [Processing](https://processing.org/) + [Reference](https://processing.org/reference/) + [on Github](https://github.com/processing)
* P5js (Github: https://github.com/processing/p5.js)
* [Sublime Text](https://www.sublimetext.com/) (éditeur de code - Mais Processing en a un...)
* [Atom.io](https://atom.io/)
* Gimp
* Inkscape
* jsFiddle
* GraphViz
* Markdown (syntaxe) - [Plus d'info](https://github.com/nbieva/getting-started/wiki/Markdown)

## Bibliothèques intéressantes..

* Pour [exporter au format PDF](https://processing.org/reference/libraries/pdf/index.html)
* Pour capturer/travailler/interagir avec du son : https://processing.org/reference/libraries/sound/index.html
* Pour capturer/travailler/interagir avec de la video : https://processing.org/reference/libraries/video/index.html
* Pour créer une interface et contrôler vos sketches en temps réel : http://www.sojamo.de/libraries/controlP5/
* Pour analyser/créer/manipuler du texte, la championne de la littérature computationnelle, RiTa : https://rednoise.org/rita/
* Pour la typographie : http://code.andreaskoller.com/libraries/fontastic/
* Pour.... tout le reste : https://processing.org/reference/libraries/#gui
* Et bien d'autres encore..
* [P5js](https://p5js.org/) est une bibliothèque JavaScript, basée sur la philosophie et l'interface de Processing. Le langage diffère légèrement mais offre l'immense intérêt de ne devoir installer aucun programme ou plugin pour jouer vos sketches et d'être interprété directement par le navigateur. Vous n'aurez aucune difficulté à passer de Processing à ceci.
* [Processing for Android](http://android.processing.org/)

## Tutoriels

* Learning Processing(http://learningprocessing.com/
* Processing reference
* https://www.youtube.com/user/shiffman
* P5js reference(https://p5js.org/reference/
* Floss manuals(https://fr.flossmanuals.net/processing/introduction/ ou à cette adresse(http://flossmanuals.developpez.com/tutoriels/processing/
* https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
* Binary numbers(https://youtu.be/l1SjnIuYPGg?list=PLG8vJUg0ALmssb3E-Yekn314MyeVCX2-7
* Sound
* OSM
* Initiation numérique (OC(https://openclassrooms.com/courses?q=&idx=prod_v2_COURSES_fr&p=0&fRcertificatetrue&fRisWebtrue&hFRcategory.nameInitiation%20num%C3%A9rique
* pages Github(http://christopheducamp.com/2013/12/21/demarrer-avec-pages-github/
* http://p5art.tumblr.com/tutorials
* Ligne de commande
* Floss
* Où apprendre?

## Liens

* Arduino(http://www.arduino.org/ + TED(https://www.youtube.com/watch?v=UoBUXOOdLXY
* http://playingwithpixels.gildasp.fr/
* https://vimeo.com/7878518
* http://beta.nodebox.net/
* https://fr.wikipedia.org/wiki/Logiciel_libre
* http://www.rtqe.net/ObliqueStrategies/
* http://recodeproject.com/
* An Informal Catalogue of Slit-Scan Video Artworks and Research(http://www.flong.com/texts/lists/slit_scan/
* Rappel sur les librairies
* Hello World!(https://vimeo.com/74725118, un film à propos de Processing

## Autres liens

* Couleurs du web(https://fr.wikipedia.org/wiki/Couleur_du_Web et Gestion des couleurs(http://www.guide-gestion-des-couleurs.com/introduction-gestion-des-couleurs.html
* Le métier Jacquard(https://fr.wikipedia.org/wiki/M%C3%A9tier_Jacquard + ceci(https://www.youtube.com/watch?v=eE5wxtaIcEY
* Playing with pixels(http://playingwithpixels.gildasp.fr/

## Showcase

* http://www.aether.hu/pgl/paris300.html
* http://reas.com/
* http://jk-lee.com/aerial-bold-project/
* http://www.richardthe.com/medialab/
* http://radicalart.info/
* http://flong.com/
* [Time Remap](https://vimeo.com/7878518)