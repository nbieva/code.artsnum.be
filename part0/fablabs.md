#FabLabs

Un FabLab est un lieu, pour tous, où vous pouvez apprendre, partager vos connaissances et collaborer sur des projets. 
Vous pouvez y découvrir une série d'outils de fabrication (gravure laser, CNC, imprimantes 3D...).

Il y a des FabLabs partout dans le monde. Ils doivent suivre la charte MIT pour les FabLabs.
Ils vous aideront aussi à documenter votre travail et à partager vos tests et découvertes.

Ce que vous


<!--
<div>
#Ok, mais lequel utiliser?

####Utilisez Processing si...

+ Vous utilisez des fonctions très gourmandes en puissance de calcul (ça arrive plus vite qu'on ne le pense). 
+ Vous voulez exporter votre création au format PDF (pour l'exploiter dans Illustrator ou Inkscape par exemple)
+ Vous créez un code secret (ou le partage de votre travail n'est pas si important)

####Utilisez P5js si...

+ Vous êtes clairement orienté web. 
+ Vous voulez partager votre travail (par mail, sur les réseaux...)
+ Vous voulez manipuler le code HTML ou CSS de votre page à la volée.

Dans tous les cas, notez que vous passerez aisément de l'un à l'autre. 
Dans un premier temps en tous cas...
</div>
-->