# Showcase

## [Frankenfont](https://fathom.info/notebook/1908/)

![](https://fathom.info/uploads/2011/11/coverphoto-038.jpg)


## [Louis Eveillard, couvertures génératives](https://louiseveillard.com/projets/couvertures-generatives)

![](https://louiseveillard.com/thumbs/projets/couvertures-generatives/couvertures_generatives-cover-1600x1167.jpg)

## [Google Book](http://www.felixheyes.com/Google-Book)

![](https://payload167.cargocollective.com/1/11/371771/5645158/GOOGLE-BOOK-open_905.jpg)

## [Stipplegem](http://www.evilmadscientist.com/2012/stipplegen2/)

![](http://farm8.staticflickr.com/7099/7183000785_a2ec65179e.jpg)

## [Texturing](http://ivan-murit.fr/43.htm)

![](http://ivan-murit.fr/works/texturing/6.jpg)

## [Object #5](http://mariuswatz.com/)

Object #5 is a parametric form developed with reference to previous 3D printed objects. 
The model was created in Processing and the parts CNC milled.

![Marius Watz](/assets/watz.jpg)


## [Le Tricodeur](https://louiseveillard.com/projets/le-tricodeur)

![](https://louiseveillard.com/thumbs/projets/le-tricodeur/tricodeur-residence-large-1-1600x1067.jpg)

[![](/assets/tricodeur2.png)](http://letricodeur.com/)

## [Atelier cartographique](https://www.atelier-cartographique.be/index.html)

![](https://www.atelier-cartographique.be/images/mons-1600.jpeg)

## [Claire Williams](http://www.xxx-clairewilliams-xxx.com/\)

![](http://www.xxx-clairewilliams-xxx.com/site/assets/files/1046/moteur_zoom.png)

## [Olivier van Herpt](http://oliviervanherpt.com/)

![](http://talent.stimuleringsfonds.nl/2016/site/assets/files/1119/olivier3.jpg)

[Solid vibration](http://oliviervanherpt.com/solid-vibrations/)

## 

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/8nTFjVm9sTQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Radiohead, House of cards ( http://www.aaronkoblin.com/project/house-of-cards/ )

---
<iframe width="560" height="315" src="https://www.youtube.com/embed/nnhJ1841K-8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

http://www.aaronkoblin.com/project/this-exquisite-forest/

---


![](http://1.bp.blogspot.com/_40xg9ErVG48/S9GD3P1lAHI/AAAAAAAABBA/JeiTHkD83AQ/s1600/DSCN3831.jpg)
Peter Schmidt, Untitled Grayscale[![](https://github.com/lam-artsnum/code/wiki/images/drawing-with-code.jpg)](https://fr.pinterest.com/nicobiev/drawing-with-code/)

[![](http://www.christies.com/lotfinderimages/D57000/victor_vasarely_paar_d5700075g.jpg)](http://www.christies.com/lotfinderimages/D57000/victor_vasarely_paar_d5700075g.jpg)Victor Vasarely: Paar

![](https://textarthistory.files.wordpress.com/2015/11/darboven.png)

[Hanne Darboven](http://www.hanne-darboven.org/index3322.html?lang=en)

![](http://www.post-gazette.com/image/2016/05/13/ca0,0,1500,1079/DYISailboats.jpg)

A. Warhol, Do It Yourself Sailboats
(Paint by numbers

* Hanne Darboven
* François Morellet
* On Kawara
* Roman Opalka
* François Rouan
* Manfred Mohr
* Sol Le Witt
* Fluxus
* Josef Albers (+Itten)
* Kandinsky+Bauhaus
* Aurélie Nemours
* Niele Toroni

[![](https://www.bernardchauveau.com/114-thickbox_default/francois-rou-la-decoupe-comme-modele.jpg)](http://bit.ly/2dZfn8z)[François Rouan](http://bit.ly/2dZfn8z)

+ http://www.liaworks.com/theprojects/monochromes/
+ http://balldroppings.com/js/](http://balldroppings.com/js/

## Norman McLaren, Synchromy

+ http://www.youtube.com/watch?v=fq7m4ame3pI](http://www.youtube.com/watch?v=fq7m4ame3pI
+ http://www.youtube.com/watch?v=2VrnXw9waJI](http://www.youtube.com/watch?v=2VrnXw9waJI

## Flavien Berger (glitch)
[![](https://i.ytimg.com/vi/dqyJin_5xT8/maxresdefault.jpg)](https://www.youtube.com/watch?v=dqyJin_5xT8)

## Bitop (bits, binaire, boucle, iteration)
[![Bitop](
https://i.ytimg.com/vi/8EMDXTQxPUY/maxresdefault.jpg)](http://0xa.kuri.mu/2011/10/09/bitop-videos)

## Paul Sharits

![Paul Sharits, "Shutter Interface," 1975.](https://i.ytimg.com/vi/Syjw1hJAADg/hqdefault.jpg)
[video](https://www.youtube.com/watch?v=Syjw1hJAADg)

![](https://www.si.edu/content/tbma/images/HMSG_Sharits2_sm.jpg)

Combinaisons, permutations, additions
![Dots 1 & 2 - 1965](https://a.ltrbxd.com/resized/film-poster/2/8/2/4/7/1/282471-dots-1-2-0-230-0-345-crop.jpg)
[video](https://ubuvideo.memoryoftheworld.org/fluxfilm_27_sharits.mp4)

![Declarative Mode 3A, 1976-77](https://www.burchfieldpenney.org/image/?action=resize&m_w=1261&m_h=445&path=/files/images/objects/1988.022.3A.jpg)

## Norman McLaren
![Synchromy - Norman McLaren](https://i.ytimg.com/vi/UmSzc8mBJCM/hqdefault.jpg)

[video](https://www.youtube.com/watch?v=UmSzc8mBJCM)

## Bernard Tschumi
![](https://s-media-cache-ak0.pinimg.com/564x/16/c1/cc/16c1cc37fe29b6ceb8477100cc8ab2fe.jpg)

## Zdeněk Sýkora
![](http://www.olmuart.cz/gfx/contentimg/0400_2560.jpg)

## Et aussi
Alva Noto

- https://www.youtube.com/watch?v=Y_-hKHbO-GE

Ryoji Ikeda

- https://www.youtube.com/watch?v=k3J4d4RbeWc

Erkii Kurenniemi

+ https://www.youtube.com/watch?v=DGM9dIXGn14
+ https://www.youtube.com/watch?v=Kya0_F4R_4Y
+ https://www.youtube.com/watch?v=xOz7uMM0lLo
+ https://www.youtube.com/watch?v=r67d2DUXVm0
+ https://www.youtube.com/watch?v=Sj2733mvbow

## Vasulkas: Computer Images

+ https://www.youtube.com/watch?v=u9bPsYWUGS0

## Brion Gysin

+ https://www.youtube.com/watch?v=Hw9dmLCdgyI

+ http://radicalart.info/concept/brecht/index.html

Dan Flavin
Kelly Ellsworth
Bridget Riley
Karl gerstner

Max Bill : http://thinkingform.com/2012/12/24/thinking-max-bill-12-22-1908

## [On Kawara](http://www.widewalls.ch/artist/on-kawara/)

![On Kawara](http://65.media.tumblr.com/3796531ebd3f9359421918000f7c428a/tumblr_n8jdb0fnV71rgnm98o1_1280.jpg)

![](http://md1.libe.com/photo/662299-card31.jpg?modified_at=1405084608&width=750)

![](https://s-media-cache-ak0.pinimg.com/originals/71/2b/e4/712be4e0d6571a0dbafde5c981605168.jpg)

![](http://www.artwiki.fr/files/OnKawara/onkawaratwitter_20141205151450_20150125161413.jpg)

## Références

#### [Lafkon](http://research.lafkon.net/projects/777/)

![](http://research.lafkon.net/content/2.projects/3.lac2008/100.plakatlac2008_A3_48.gif)

![](http://research.lafkon.net/content/2.projects/7.777/69.chmod+x_nl--051.gif)

## Lust, [Lost and Found]()
![](https://lust.nl/media/image/large/LostFound_m.jpg)

## Julien Maire
![](http://www.makery.info/wp-content/uploads/2014/10/1-IMG_9599.jpg)

![](http://v2.nl/events/julien-maire-man-at-work/leadImage_large)

## Karl Gertner
![](https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fpage-spread.com%2Fwp-content%2Fuploads%2F06_213.jpg)

## [Graphic Design In the White Cube](https://jonathanpuckey.com/projects/graphic-design-in-the-white-cube/)
![](https://jonathanpuckey.com/projects/graphic-design-in-the-white-cube/resources/poster-5.jpg@thumb=3f3e48a2c865b2e8703ab474a9df4af5)

## [Jonathan Puckey](https://jonathanpuckey.com/projects/lettering-tool)

![](https://jonathanpuckey.com/projects/lettering-tool/resources/cover.jpg@thumb=3f3e48a2c865b2e8703ab474a9df4af5)


# Bouncing ball
+ https://www.youtube.com/watch?v=jLuMjfxgBpc
