#Les différents types de variables
>Source: [Floss manuals](https://fr.flossmanuals.net/processing/les-variables/)

Une variable est une donnée que l'ordinateur va stocker dans l'espace de sa mémoire. C'est comme un compartiment dont la taille n'est adéquate que pour un seul type d'information. Elle est caractérisée par un nom qui nous permettra d'y accéder facilement.

![illustration_variables_memoire_1.png](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-illustration_variables_memoire_1-fr-old.png)

Il existe différents type de variables : des nombres entiers (int), des nombres à virgule (float), du texte (String), des valeurs vrai/faux (boolean). Un nombre à décimales, comme 3,14159, n'étant pas un nombre entier, serait donc du type float. Notez que l'on utilise un point et non une virgule pour les nombres à décimales. On écrit donc 3.13159. Dans ce cas, les variables peuvent être annoncées de cette manière :
```processing
float x = 3.14159;
int y = 3;
```
Le nom d'une variable peut contenir des lettres, des chiffres et certains caractères comme la barre de soulignement. À chaque fois que le programme rencontre le nom de cette variable, il peut lire ou écrire dans ce compartiment. Les variables qui vont suivre vous donneront des exemples simples de leur utilisation. Pour résumer, une variable aura un type, un nom et une valeur qui peut être lue ou modifiée.

##int
Dans la syntaxe de Processing, on peut stocker un nombre entier, par exemple 3, dans une variable de type int.
```processing
int entier;
entier = 3;
print(entier);
```
![var_int.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-var_int-tiff-fr-old.png)

##float

Il s'agit d'un nombre avec décimales, par exemple 2,3456.
```processing
float decimal;
decimal = PI;
print(decimal);
```
![float_var.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-float_var-tiff-fr-old.png)
##double

Il s'agit également de nombre avec décimales, mais qui fournissent davantage de précision que le type float.
```processing
double long_decimal;
long_decimal = PI;
print(long_decimal);
```
![double_var.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-double_var-tiff-fr-old.png)

##boolean

Il s'agit d'un type de variable qui ne connaît que deux états : Vrai (true) ou Faux (false). Elle est utilisée dans les conditions pour déterminer si une expression est vraie ou fausse.
```processing
boolean vraifaux;
vraifaux = true;
println(vraifaux);
```
![char_bool](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-char_bool-fr-old.jpg)

##char

Cette variable sert à stocker un caractère typographique (une lettre). Notez l'usage de ce qu'on appelle des guillemets simples.
```processing
char lettre;
lettre = 'A';
print(lettre);
```
![char_var.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-char_var-tiff-fr-old.png)

##string

Cette variable sert à stocker du texte. Notez l'usage des guillemets doubles.
```processing
String texte;
texte = "Bonjour!";
print(texte);
```
![string_var.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-string_var-tiff-fr-old.png)

##color

Sert à stocker une couleur. Cette variable est utile lorsqu'on veut réutiliser souvent les mêmes couleurs.

![damier.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-damier-tiff-fr-old.png)
```processing
noStroke();
color blanc = color(255, 255, 255);
color noir = color(0, 0, 0);

fill(blanc); rect(0, 0, 25, 25);
fill(noir); rect(25, 0, 25, 25);
fill(blanc); rect(50, 0, 25, 25);
fill(noir); rect(75, 0, 25, 25);

fill(noir); rect(0, 25, 25, 25);
fill(blanc); rect(25, 25, 25, 25);
fill(noir); rect(50, 25, 25, 25);
fill(blanc); rect(75, 25, 25, 25);

fill(blanc); rect(0, 50, 25, 25);
fill(noir); rect(25, 50, 50, 25);
fill(blanc); rect(50, 50, 75, 25);
fill(noir); rect(75, 50, 100, 25);

fill(noir); rect(0, 75, 25, 25);
fill(blanc); rect(25, 75, 25, 25);
fill(noir); rect(50, 75, 25, 25);
fill(blanc); rect(75, 75, 25, 25);
```