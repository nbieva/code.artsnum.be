#Typographie

Le travail du texte dans Processing peut vous mener très loin. Cette page reprend quelques ressources et tutoriels plus avancés, mais pouvant être mis en oeuvre assez rapidement. nous vous invitons à prendre le temps de les parcourir, de tester les bouts de code proposés, et de les modifier.
En effet, à l'instar de ce qui se fait pour le traitement de la video, l'export PDF ou la création d'interfaces utilisateur, une série de bibliothèques (libraries) proposent un grand nombre de fonctions avancées pour la manipulation du texte.

Pour aller plus loin:

+ http://laikafont.ch/
+ **http://basiljs.ch/about/**
+ **https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn**
+ https://www.openprocessing.org/sketch/161029
+ http://code.andreaskoller.com/libraries/fontastic/
+ http://www.ricardmarxer.com/geomerative/
+ https://www.plagiairc.com/
+ http://brunoimbrizi.com/unbox/2011/10/processing-typography/
+ https://www.creativeapplications.net/processing/generative-typography-processing-tutorial/
+ **http://jk-lee.com/aerial-bold-project/**

![typo](/assets/typo1.png) 
Dans [*Generative Typography with Processing – Tutorial*](https://www.creativeapplications.net/processing/generative-typography-processing-tutorial/), written by **Amnon Owed**

[Type + Code: Processing For Designers](https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn) (Published on May 29, 2009)  
By Yeohyun Ahn and Viviana Cordova. Type + Code, explores the aesthetic of experimental code driven typography, with an emphasis on the programming language Processing which was created by Casey Reas and Ben Fry.