#Quelques exemples de base

## Une ellipse

```processing
void setup() {
    size(600,400);
    background(200,200,200);
    ellipse(200,200,100,100);
}
```

## Une ellipse centrée

Une première utilisation de variables natives (width, height, mouseX, mouseY, frameCount, ...).

```processing
void setup() {
    size(600,400);

    // On définit une couleur de fond
    background(200,200,200);

    //Je supprime les contours pour les objets qui vont être dessinés
    noStroke();

    //On modifie la couleur de remplissage
    fill(255,255,0);

    // On dessine l'ellipse
    ellipse(width/2,height/2,100,100);
}
```

## Une ellipse qui suit votre souris

Dans ce cas, pour suivre la souris, l'ellipse doit être redessinée à chaque boucle, après avoir récupéré les positions en X et Y de votre souris.

```processing
void setup() {
    size(600,400);
    background(200,200,200);
    rect(50,50,width-100,height-100);
}
void draw() {
    fill(255,255,0);
    ellipse(mouseX,mouseY,50,50);
}
```

## Une autre, avec un rectangle de fond ajouté à chaque boucle

Dans ce cas, pour suivre la souris, l'ellipse doit être redessinée à chaque boucle, après avoir récupéré les positions en X et Y de votre souris.

```processing
void setup() {
    size(600,400);
}
void draw() {
    background(200,200,200);
    fill(255,255,0);
    ellipse(mouseX,mouseY,50,50);
}
```

## Un peu d'aléatoire

Dans ce cas, pour suivre la souris, l'ellipse doit être redessinée à chaque boucle, après avoir récupéré les positions en X et Y de votre souris. Vous trouverez plus d'information concernant **random()** [sur cette page](http://localhost:4000/part2/aleatoire.html).

![](/assets/Capture d'écran 2018-02-18 22.44.44.png)

```processing
void setup() {
    size(600,400);
    background(150,100,230);
    noStroke();
}
void draw() {
    fill(255,255,0);
    ellipse(random(width),random(height),10,10);
}
```

## Création de variables

Voici une simple utilisation de variable. Les étapes sont les suivantes :

* **Déclarer** la variable
* Lui **assigner** une valeur
* L'**utiliser**
* La **modifier** (à chaque boucle par exemple)



```processing
//On déclare la variables X on lui assigne la valeur 0
float posX = 0;

void setup() {
    size(700,350);
    background(200,100,100);
}
void draw() {
    fill(255,255,0);
    ellipse(posX,height/2,20,20);
    //A chaque boucle, la position en X augmentent de 1. L'ellipse se déplace.
    posX = posX+10; // ou posX += 1
}
```