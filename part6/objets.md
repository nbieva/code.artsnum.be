#Les objets

L'objet, en programmation, est un sujet qui est bien au-delà de ce que nous pouvons faire ajourd'hui.
Néanmoins, si certains d'entre vous se sentent l'âme aventurière, voici un bout de code qui devrait vous servir d'indice, d'appât, ou de piège...

![](/assets/objects.png)

```processing
Point[] points;

int sensY = 1;
int sensX = 1;
float epaisseur;

void setup() {
    background(255);
    size(800,600);
    //fullScreen();
    int numPoints = 25;
    points = new Point[numPoints];
    for (int i=0; i<points.length;i++) {
        epaisseur = random(10,20);
        points[i] = new Point(random(width),random(height),epaisseur, random(2,5), color(random(20),random(100),random(65)));
    }
}

void draw() {
    fill(165, 169, 157,20);
    noStroke();
    rect(0,0,width,height);
    for (int i=0; i<points.length;i++) {
    points[i].deplace();
    }
}
/*
void keyPressed() {
    save("screenshot.png");
    exit();
}
void mouseReleased() {
    loop();
}
void mousePressed() {
    noLoop();
}
*/
class Point {
    float x, y, dia, speed;
    color myColor;
    int direction = 1;
    int laterale = 1;
    String texte;
    Point(float xpos, float ypos, float diametre, float sp, color maCouleur) {
        x = xpos;
        y = ypos;
        dia = diametre;
        speed = sp;
        myColor = maCouleur;
    }
    void deplace() {
        float newSpeed;
        y += random(-0.7,0.7);//(speed * direction);
        x += (speed *laterale);
        if ((y > (height-dia/2)) || (y < dia/2)) {
            direction *= -1;
            sensY = sensY*(-1);
        }
        if ((x > (width-dia/2)) || (x < dia/2)) {
            laterale *= -1;
            sensX = sensX*(-1);
        }
        strokeWeight(dia);
        stroke(myColor);
        point(x,y);
    }
}
```

