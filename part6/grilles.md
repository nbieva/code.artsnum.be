#Une grille simple

```processing
int grid = 10; // écartement entre les lignes

void setup() {
    // dimension du canvas
    size(800,600);
    // fond blanc
    background(255);

    // grille verticale
    for (int i = 10; i < width; i+=grid) { // on incrémente i de la valeur d'écartement définie à la première ligne
        line (i, 0, i, height); // chaque ligne se positionne en x selon la valeur de i
    }
    // grille horizontale
    for (int i = 10; i < height; i+=grid) { // on incrémente i de la valeur d'écartement définie à la première ligne
        line (0, i, width, i); // chaque ligne se positionne en y selon la valeur de i
    }
}

```

![](/assets/grille.png)

## Un peu plus "flexible"..

A peu près la même chose, mais dans le sketch ci-dessous, on remplace la valeur initiale de "i" par la variable "grid". De cette manière vous pourrez plus aisément modifier la taille de vos carrés. La grille se mettra à jour.


```processing
int grid = 10;

void setup() {
    size(800,600);
    background(255);

    // On remplace la valeur initiale de "i" par la variable "grid"
    for (int i = grid; i < width; i+=grid) {
        line (i, 0, i, height);
    }
    // grille horizontale
    for (int i = grid; i < height; i+=grid) {
        line (0, i, width, i);
    }
}

```






