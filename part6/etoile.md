#star(), une fonction personnalisée

![](/assets/etoile.png)

Le code ci-dessous vient de la [référence sur le site de Processing](https://processing.org/examples/star.html). Il nous montre l'utilisation d'une fonction personnalisée.

Il n'y a pas de fonction star() par défaut dans Processing. En voici une, créée pour palier à ce manque. Notez qu'il y a ce type de code derrière chaque fonction de Processing (rect, stroke, fill, etc..).

La fonction ci-dessous prends un certain nombre d'arguments (paramètres de la fonction). A vous de les identifier et de trouver à quoi ils correspondent. La meilleure façon de faire est peut-être de les modifier et d'analyser le résultat...

**Important:** Notez que le bloc **star()** (la nouvelle fonction) se met au même niveau que **setup()** et **draw()**.


```processing
void setup() {
    size(640, 360);
}

void draw() {
    background(102);
    pushMatrix();
    translate(width*0.2, height*0.5);
    rotate(frameCount / 200.0);
    star(0, 0, 5, 70, 3);
    popMatrix();
    pushMatrix();
    translate(width*0.5, height*0.5);
    rotate(frameCount / 400.0);
    star(0, 0, 80, 100, 40);
    popMatrix();
    pushMatrix();
    translate(width*0.8, height*0.5);
    rotate(frameCount / -100.0);
    star(0, 0, 30, 70, 5);
    popMatrix();
}

void star(float x, float y, float radius1, float radius2, int npoints) {
    float angle = TWO_PI / npoints;
    float halfAngle = angle/2.0;

    beginShape();
    for (float a = 0; a < TWO_PI; a += angle) {
        float sx = x + cos(a) * radius2;
        float sy = y + sin(a) * radius2;
        vertex(sx, sy);
        sx = x + cos(a+halfAngle) * radius1;
        sy = y + sin(a+halfAngle) * radius1;
        vertex(sx, sy);
    }
    endShape(CLOSE);
}
```

