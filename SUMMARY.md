# Summary

## Workshop B1

* [Une filiation](part0/filiation.md)
* [Exemples](part0/exemples.md)
* [Processing & P5js](part0/processing-et-p5js.md)
* [Arduino & RaspberryPi](part0/monde-physique.md)
* [Scratch & Micro:bit](part0/scratch.md)
* [Rendus de vos travaux](part7/rendus-des-travaux.md)
* [Formulaire](part0/formulaire.md)

## Processing
* [Structure d'un programme](part1/structure-dun-programme.md)
* [Les coordonnées](part1/les-coordonnees.md)
* [Dessin de formes](part1/formes-simples.md)
* [La couleur](part1/couleur.md)
* [Les transformations](part1/transformations.md)
* [Les variables](part2/variables.md)
* [Les types de variables](part2/variables-2.md)
----
* [Les conditions](part2/conditions.md)
* [Les boucles](part2/boucles.md)
* [Les méthodes](part2/methodes.md)

##Plus spécifiquement...
* [Mouvement](part2/animation.md)
* [Nombres aléatoires](part2/aleatoire.md)
* [Le texte](part2/texte.md)
* [Typographie](part2/typographie.md)
* [Les images](part2/images.md)
* [Export PDF](part2/export-pdf.md)

##Pistes pour la journée
* [Croquis/sketch (1)](part4/croquis.md)
* [Croquis/sketch (2)](part4/ex-formessimples.md)
* [Horloge](part4/horloge.md)
* [Un outil de dessin](part4/ex-outil-de-dessin.md)
* [Trame simple](part4/ex-trame-simple.md)
* [Trame et variantes](part4/ex-trame-et-variantes.md)
* [Slit scan](part4/slitscan.md)
* [Mickey (Damian Hirst)](part4/mickey.md)

##Exemples de codes
* [Quelques exemples de base](part6/quelques-exemples-de-base.md)
* [Une étoile](part6/etoile.md)
* [Les grilles](part6/grilles.md)
* [Une grille de points](part6/grille-points.md)
* [Interaction avec la souris](part6/interaction-avec-la-souris.md)
* [Les objets](part6/objets.md)

##Exports de vos travaux
* [Exporter des images](part7/exports.md)
* [Exporter en video](part7/video.md)
* [Export PDF avancé](part7/export-pdf-avance.md)